# TG4 Validations n Pipes (0.1v)

Módulos Utilitários para Javascript & Angular 4.  
Validações para Form Builder & Pipes de Visualização de Dados.

### Instalação & Inicialização

#### Inserir no package.json:

	{ 
		(...)
		"tg4-validations_n_pipes": "git+ssh://git@bitbucket.org:TG4IT/tg4-validations_n_pipes.git"
		(...)
	}

	npm install

#### Inserir no app:

	import { PipesModule, ValidationModule } from 'tg4-validations_n_pipes';

	@NgModule({
		(...)
		imports: [
		  (...)
		  PipesModule,
		  ValidationModule
	  ]
	  (...)
	})

### Utilização

#### Validações

Registrando os Métodos de Validação no FormGroup:

	const formGroup = new FormGroup({
    'dados': new FormGroup({
    	'cpf': new FormControl([
    		ValidationService.validarCPF,
    		ValidationService.retirarLetras
    	]),
      'email': new FormControl([
      	ValidationService.validarEmail
    	]),
      'cep': new FormControl([
      	ValidationService.validarCep,
      	ValidationService.retirarLetras
    	])
    })
	});

Incluindo o Control-Message com o Erro de Validação:

	<div [formGroupName]="'dados'">
		<input type="tel" name="cpf" formControlName="cpf" />
		<control-messages [control]="formGroup.get('dados.cpf')"></control-messages>
	</div>

Lista de Métodos de Validação:

VALIDAÇÃO DE DADOS BÁSICOS  
- validarEmail  
- validarNome  
- validarCep  
- validarTelefoneCelular  
- validarTelefoneFixo

VALIDAÇÃO DE DOCUMENTOS  
- validarCPF  
- validarCNPJ  
- validarPisPasep  
- validarDeclaracaoNascidoVivo  
- validarCartaoNacionalSaude

CLEANERS  
- retirarNumeros  
- retirarNumerosMenosTraco  
- retirarCaracterEspecialMenosTraco  
- retirarCaracterEspecial  
- retirarLetras  
- transformarLetrasAcentuadas

#### Pipes

Utilizar normalmente como um pipe do Angular.

	CPF:  {{ '12316560857' | cpfFormat }} 
	CNPJ: {{ '48251874000148' | cnpjFormat }} 
	CEP:  {{ '30510410' | cepFormat }}
	R$:   {{ 100 | currencyFormat : 'R$ ' : '.' : ',' }}
	// (Símbolo da Moeda : Divisor de Milhar : Divisor de Decimal)
