export function retirarNumeros(control: any): any
export function retirarNumerosMenosTraco(control: any): any
export function retirarCaracterEspecialMenosTraco(control: any): any
export function retirarCaracterEspecial(control: any): any
export function retirarLetras(control: any): any
export function transformarLetrasAcentuadas(control): any
