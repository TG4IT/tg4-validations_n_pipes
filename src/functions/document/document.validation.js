export function validarCNPJ(cnpjStr) {
    
    const cnpj = cnpjStr.replace(/[^\d]+/g, '');
    let i = 0;
    let l = 0;
    let strNum = '';
    const strMul = '6543298765432';
    let character = '';
    let iValido = 1;
    let iSoma = 0;
    let strNum_base = '';
    let iLenNum_base = 0;
    let iLenMul = 0;

    if (cnpj !== '') {

        if ((cnpj === '00000000000000') || (cnpj === '11111111111111') || (cnpj === '22222222222222') || (cnpj === '33333333333333') || (cnpj === '44444444444444') ||
            (cnpj === '55555555555555') || (cnpj === '66666666666666') || (cnpj === '77777777777777') || (cnpj === '88888888888888') || (cnpj === '99999999999999'))
            return false;

        l = cnpj.length;
        for (i = 0; i < l; i++) {
            var caracter = cnpj.substring(i, i + 1);
            if ((caracter >= '0') && (caracter <= '9'))
                strNum = strNum + caracter;
        };

        if (strNum.length !== 14)
            return false;

        strNum_base = strNum.substring(0, 12);
        iLenNum_base = strNum_base.length - 1;
        iLenMul = strMul.length - 1;
        for (i = 0; i < 12; i++)
            iSoma = iSoma +
                parseInt(strNum_base.substring((iLenNum_base - i), (iLenNum_base - i) + 1), 10) *
                parseInt(strMul.substring((iLenMul - i), (iLenMul - i) + 1), 10);

        iSoma = 11 - (iSoma - Math.floor(iSoma / 11) * 11);
        if (iSoma === 11 || iSoma === 10)
            iSoma = 0;

        strNum_base = strNum_base + iSoma;
        iSoma = 0;
        iLenNum_base = strNum_base.length - 1;

        for (i = 0; i < 13; i++)
            iSoma = iSoma + parseInt(strNum_base.substring((iLenNum_base - i), (iLenNum_base - i) + 1), 10) * parseInt(strMul.substring((iLenMul - i), (iLenMul - i) + 1), 10);

        iSoma = 11 - (iSoma - Math.floor(iSoma / 11) * 11);
        if (iSoma === 11 || iSoma === 10)
            iSoma = 0;
        strNum_base = strNum_base + iSoma;

        if (strNum !== strNum_base)
            return false
        return true;
    }
}

export function validarCPF(cpf) {

    const regex = /[.-\s]/g;
    let strCPF = cpf.replace(regex, '');
    let Soma = 0;
    var Resto;

    if ((strCPF === '00000000000') || (strCPF === '11111111111') || (strCPF === '22222222222') || (strCPF === '33333333333') || (strCPF === '44444444444') ||
        (strCPF === '55555555555') || (strCPF === '66666666666') || (strCPF === '77777777777') || (strCPF === '88888888888') || (strCPF === '99999999999'))
        return false;

    for (let i = 1; i <= 9; i++) {
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;
    }

    if ((Resto === 10) || (Resto === 11))
        Resto = 0;

    if (Resto !== parseInt(strCPF.substring(9, 10)))
        return false;

    Soma = 0;
    for (let i = 1; i <= 10; i++) {
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;
    }

    if ((Resto === 10) || (Resto === 11))
        Resto = 0;

    if (Resto !== parseInt(strCPF.substring(10, 11)))
        return false;

    return true;
}

export function validarPisPasep(pisPasepStr) {
    return validarDocumento(numeroStr, 11);
}

export function validarDeclaracaoNascidoVivo(numeroStr) {
    return validarDocumento(numeroStr, 11);
}

export function validarCartaoNacionalSaude(numeroStr) {
    return validarDocumento(numeroStr, 15);
}

function validarDocumento(numeroStr, docLength) {
    const numero = numeroStr.replace(/[.-\s]/g, '');
    return numero.trim() === '' || numero.length !== docLength;
}
