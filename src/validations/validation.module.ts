import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ControlMessages } from './control-messages.component';
import { ValidationService } from './validation.service';

@NgModule({
  declarations: [ControlMessages],
  imports: [BrowserModule],
  exports: [ControlMessages],
  providers: [ValidationService]
})
export class ValidationModule { }
