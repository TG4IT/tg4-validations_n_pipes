import { Injectable } from '@angular/core';

import {
    validarCPF,
    validarCNPJ,
    validarPisPasep,
    validarDeclaracaoNascidoVivo,
    validarCartaoNacionalSaude
} from '../functions/document/document.validation';

import { 
    validarEmail,
    validarNome,
    validarCep,
    validarTelefoneCelular,
    validarTelefoneFixo
} from '../functions/user-info/user-info.validation';

import {
    retirarNumeros,
    retirarNumerosMenosTraco,
    retirarCaracterEspecialMenosTraco,
    retirarCaracterEspecial,
    retirarLetras,
    transformarLetrasAcentuadas
} from '../functions/cleaners/cleaners';

@Injectable()
export class ValidationService {

    constructor() { }

    public static getValidatorErrorMessage(validatorName: string, validatorValue?): string {
        let config = {
            'required': 'Campo Obrigatório.',
            'invalidCpf': 'Digite um CPF válido.',
            'invalidPeriod': 'A data escolhida deve ser menor que a data de hoje.',
            'invalidEmail': 'E-mail inválido.',
            'invalidCep': 'Formato de CEP inválido.',
            'requiredCheckBox': 'Campo obrigatório',
            'minlength': `Tamanho mínimo ${validatorValue.requiredLength}.`,
            'maxlength': `Tamanho máximo ${validatorValue.requiredLength}.`,
            'lengthNameError': 'Digite seu nome completo.',
            'invalidTel': 'Telefone inválido.',
            'invalidCnpj': 'O CNPJ informado é inválido. Por favor, verifique e atualize a informação.',
            'nameError': 'Digite o nome corretamente.',
            'invalidText': 'O texto não pode conter caracter especial.',
            'invalidPassword': 'A senha deve ter mais do que 6 caracteres e conter letra e número.',
            'textError': 'Somente letras são permitidas',
            'invalidDeclaracaoNascidoVivo': 'Digite um nº de Declaração Nascido Vivo válido.',
            'invalidCartaoNacionalSaude': 'Digite um nº de Cartão Nacional de Saúde válido.',
            'invalidPisPasep': 'Digite um nº de PIS/PASEP válido.'
        };

        return config[validatorName];
    }

    public static validarCNPJ(control) {
        if (verificarControlInvalido(control)) return null;
        return validarCNPJ(control.value) ? null : { 'invalidCnpj': true };
    }

    public static validarCPF(control) {
        if (verificarControlInvalido(control)) return null;
        return validarCPF(control.value) ? null : { 'invalidCpf': true };
    }

    public static validarPisPasep(control) {
        if (verificarControlInvalido(control)) return null;
        return validarPisPasep(control.value) ? null : { 'invalidPisPasep': true };
    }

    public static validarDeclaracaoNascidoVivo(control) {
        if (verificarControlInvalido(control)) return null;
        return validarDeclaracaoNascidoVivo(control.value) ? null : { 'invalidDeclaracaoNascidoVivo': true };
    }

    public static validarCartaoNacionalSaude(control) {
        if (verificarControlInvalido(control)) return null;
        return validarCartaoNacionalSaude(control.value) ? null : { 'invalidCartaoNacionalSaude': true };
    }

    public static validarEmail(control) {
        if (verificarControlInvalido(control)) return null;
        return validarEmail(control.value) ? null : { 'invalidEmail': true };
    }

    public static validarNome(control) {
        if (verificarControlInvalido(control)) return null;

        const nome = control.value.replace(/[^a-zA-ZáéíóúàèìòùâêîôûãõçÁÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃÕÇ\s]/g, '');
        if (control.value !== nome)
          control.setValue(nome);

        return validarEmail(nome) ? null : { 'nameError': true };
    }

    public static validarCep(control) {
        if (verificarControlInvalido(control)) return null;
        return validarEmail(control.value) ? null : { 'invalidCep': true };
    }

    public static retirarNumeros(control) {
        if (verificarControlInvalido(control)) return null;
        const valor = retirarNumeros(control.value);
        atualizarControl(control, valor);
    }

    public static retirarNumerosMenosTraco(control) {
        if (verificarControlInvalido(control)) return null;
        const valor = retirarNumerosMenosTraco(control.value);
        atualizarControl(control, valor);
    }

    public static retirarCaracterEspecialMenosTraco(control) {
        if (verificarControlInvalido(control)) return null;
        const valor = retirarCaracterEspecialMenosTraco(control.value);
        atualizarControl(control, valor);
    }

    public static retirarCaracterEspecial(control) {
        if (verificarControlInvalido(control)) return null;

        const nome = retirarCaracterEspecial(control.value);
        if (nome && (control.value !== nome))
            control.patchValue(nome, { emitEvent: false });
    }

    public static retirarLetras(control) {
        if (verificarControlInvalido(control)) return null;
        const valor = retirarLetras(control.value);
        atualizarControl(control, valor);
    }

    public static transformarLetrasAcentuadas(control) {
        if (verificarControlInvalido(control)) return null;
        return transformarLetrasAcentuadas(control.value);
    }

    public static validarTelefoneCelular(control) {
        if (verificarControlInvalido(control)) return null;
        return validarTelefoneCelular(control.value) ? null : { 'invalidTel': true };
    }

    public static validarTelefoneFixo(control) {
        if (verificarControlInvalido(control)) return null;
        return validarTelefoneFixo(control.value) ? null : { 'invalidTel': true };
    }
}

function verificarControlInvalido(control): boolean {
    return !control || !control.value;
}

function atualizarControl(control, valor) {
    if (control.value !== valor)
        control.setValue(valor);
}
