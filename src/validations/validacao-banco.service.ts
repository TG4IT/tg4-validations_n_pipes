import { Injectable } from "@angular/core";

@Injectable()
export class ValidacaoBancoService {
    private pad(str, max) {
        str = str.toString();
        return str.length < max ? this.pad("0" + str, max) : str;
    }

    private checkAllZeros(agencia_conta: string) {
        for(let i = 0; i < agencia_conta.length; i++) {
            if(agencia_conta[i] !== '0') {
                return false;
            }
        }

        return true;
    }

    // Validações dígito verificador de Agência

    public validaDVAgenciaBancoDoBrasil(agencia, dv) {
        if (dv.length != 1) return false;

        agencia = this.pad(agencia, 4);

        var SomaDigitos =
            agencia.charAt(0) * 5 +
            agencia.charAt(1) * 4 +
            agencia.charAt(2) * 3 +
            agencia.charAt(3) * 2;
        var Resto = SomaDigitos % 11;
        var DigitoV: any = 11 - Resto;

        if (DigitoV == 10) DigitoV = "X";
        else if (DigitoV == 11) DigitoV = 0;

        return DigitoV == dv ? true : false;
    }

    public validaDVAgenciaBanrisul(agencia, dv) {
        if (dv.length != 2) return false;

        var Digito1: any = (agencia.charAt(0) * 1).toString();
        if (Digito1 > 9)
            Digito1 = parseInt(Digito1.charAt(0)) + parseInt(Digito1.charAt(1));

        var Digito2: any = (agencia.charAt(1) * 2).toString();
        if (Digito2 > 9)
            Digito2 = parseInt(Digito2.charAt(0)) + parseInt(Digito2.charAt(1));

        var Digito3: any = (agencia.charAt(2) * 1).toString();
        if (Digito3 > 9)
            Digito3 = parseInt(Digito3.charAt(0)) + parseInt(Digito3.charAt(1));

        var Digito4: any = (agencia.charAt(3) * 2).toString();
        if (Digito4 > 9)
            Digito4 = parseInt(Digito4.charAt(0)) + parseInt(Digito4.charAt(1));

        var SomaDigitos =
            parseInt(Digito1) +
            parseInt(Digito2) +
            parseInt(Digito3) +
            parseInt(Digito4);
        var Resto = SomaDigitos % 10;
        var DV1 = Resto == 0 ? 0 : 10 - Resto;

        var DV2 = this.calcula2ndDigitoBanrisul(agencia, DV1);

        if (DV1 != parseInt(dv.charAt(0))) return false;
        return DV2 == parseInt(dv.charAt(1)) ? true : false;
    }

    calcula2ndDigitoBanrisul(agencia, DV1) {
        var Digito1 = agencia.charAt(0) * 6;
        var Digito2 = agencia.charAt(1) * 5;
        var Digito3 = agencia.charAt(2) * 4;
        var Digito4 = agencia.charAt(3) * 3;
        var Digito5 = DV1 * 2;

        var SomaDigitos = Digito1 + Digito2 + Digito3 + Digito4 + Digito5;
        var Resto = SomaDigitos % 11;
        var DV2;

        if (Resto == 0) return 0;
        else if (Resto == 1 && DV1 != 9) {
            DV1++;
            return this.calcula2ndDigitoBanrisul(agencia, DV1);
        } else if (Resto == 1 && DV1 == 9) {
            DV1 = 0;
            return this.calcula2ndDigitoBanrisul(agencia, DV1);
        }

        DV2 = 11 - Resto;
        return DV2;
    }

    public validaDVAgenciaBradesco(agencia, dv) {
        if (dv.length != 1) return false;

        agencia = this.pad(agencia, 4);

        var SomaDigitos =
            agencia.charAt(0) * 5 +
            agencia.charAt(1) * 4 +
            agencia.charAt(2) * 3 +
            agencia.charAt(3) * 2;
        var Resto = SomaDigitos % 11;
        var DigitoV: any = 11 - Resto;

        if (DigitoV == 11) DigitoV = 0;
        else if (DigitoV == 10) DigitoV = "P";

        DigitoV = DigitoV.toString();
        return DigitoV == dv ? true : false;
    }

    // Validação número de dígitos agência

    public validaAgenciaBancoob(agencia) {
        if(this.checkAllZeros(agencia)) {
            return false;
        }
        return agencia.length <= 4 ? true : false;
    }

    public validaAgenciaBradesco(agencia) {
        if(this.checkAllZeros(agencia)) {
            return false;
        }
        return agencia.length <= 4 ? true : false;
    }

    public validaAgenciaItau(agencia) {
        if(this.checkAllZeros(agencia)) {
            return false;
        }
        return agencia.length <= 4 ? true : false;
    }

    public validaAgenciaBancodoBrasil(agencia) {
        if(this.checkAllZeros(agencia)) {
            return false;
        }
        return agencia.length <= 4 ? true : false;
    }

    public validaAgenciaSantander(agencia) {
        if(this.checkAllZeros(agencia)) {
            return false;
        }
        return agencia.length <= 4 ? true : false;
    }

    public validaAgenciaCaixa(agencia) {
        if(this.checkAllZeros(agencia)) {
            return false;
        }
        return agencia.length <= 4 ? true : false;
    }

    // Validação número de dígitos conta

    public validaContaBancoob(conta) {
        if(this.checkAllZeros(conta)) {
            return false;
        }
        return conta.length <= 7 ? true : false;
    }

    public validaContaBradesco(conta) {
        if(this.checkAllZeros(conta)) {
            return false;
        }
        return conta.length <= 7 ? true : false;
    }

    public validaContaItau(conta) {
        if(this.checkAllZeros(conta)) {
            return false;
        }
        return conta.length <= 5 ? true : false;
    }

    public validaContaBancodoBrasil(conta) {
        if(this.checkAllZeros(conta)) {
            return false;
        }
        return conta.length <= 8 ? true : false;
    }

    public validaContaSantander(conta) {
        if(this.checkAllZeros(conta)) {
            return false;
        }
        return conta.length <= 8 ? true : false;
    }

    public validaContaCaixa(conta) {
        if(this.checkAllZeros(conta)) {
            return false;
        }
        return conta.length <= 11 ? true : false;
    }

    // Validação dígito verificador conta-corrente

    public validaDVContaBancoDoBrasil(conta, dv) {
        if (conta.length < 8) {
            var padding = new Array(9 - conta.length).join("0");
            conta = padding + conta;
        }

        conta = this.pad(conta, 8);

        var SomaDigitos =
            conta.charAt(0) * 9 +
            conta.charAt(1) * 8 +
            conta.charAt(2) * 7 +
            conta.charAt(3) * 6 +
            conta.charAt(4) * 5 +
            conta.charAt(5) * 4 +
            conta.charAt(6) * 3 +
            conta.charAt(7) * 2;

        var Resto = SomaDigitos % 11;
        var DigitoV: any = 11 - Resto;

        if (DigitoV == 11) DigitoV = 0;
        else if (DigitoV == 10) DigitoV = "X";

        DigitoV = DigitoV.toString();
        return DigitoV == dv ? true : false;
    }

    public validaDVContaSantander(agencia, conta, dv) {
        agencia = this.pad(agencia, 4);
        conta = this.pad(conta, 8);
        var Digito1: any = (agencia.charAt(0) * 9).toString();
        if (Digito1 > 9) Digito1 = parseInt(Digito1.charAt(1));
        var Digito2: any = (agencia.charAt(1) * 7).toString();
        if (Digito2 > 9) Digito2 = parseInt(Digito2.charAt(1));
        var Digito3: any = (agencia.charAt(2) * 3).toString();
        if (Digito3 > 9) Digito3 = parseInt(Digito3.charAt(1));
        var Digito4: any = (agencia.charAt(3) * 1).toString();
        if (Digito4 > 9) Digito4 = parseInt(Digito4.charAt(1));

        var Digito5: any = (conta.charAt(0) * 9).toString();
        if (Digito5 > 9) Digito5 = parseInt(Digito5.charAt(1));
        var Digito6: any = (conta.charAt(1) * 7).toString();
        if (Digito6 > 9) Digito6 = parseInt(Digito6.charAt(1));
        var Digito7: any = (conta.charAt(2) * 1).toString();
        if (Digito7 > 9) Digito7 = parseInt(Digito7.charAt(1));
        var Digito8: any = (conta.charAt(3) * 3).toString();
        if (Digito8 > 9) Digito8 = parseInt(Digito8.charAt(1));
        var Digito9: any = (conta.charAt(4) * 1).toString();
        if (Digito9 > 9) Digito9 = parseInt(Digito9.charAt(1));
        var Digito10: any = (conta.charAt(5) * 9).toString();
        if (Digito10 > 9) Digito10 = parseInt(Digito10.charAt(1));
        var Digito11: any = (conta.charAt(6) * 7).toString();
        if (Digito11 > 9) Digito11 = parseInt(Digito11.charAt(1));
        var Digito12: any = (conta.charAt(7) * 3).toString();
        if (Digito12 > 9) Digito12 = parseInt(Digito12.charAt(1));

        var SomaDigitos =
            parseInt(Digito1) +
            parseInt(Digito2) +
            parseInt(Digito3) +
            parseInt(Digito4) +
            parseInt(Digito5) +
            parseInt(Digito6) +
            parseInt(Digito7) +
            parseInt(Digito8) +
            parseInt(Digito9) +
            parseInt(Digito10) +
            parseInt(Digito11) +
            parseInt(Digito12);
        if (SomaDigitos > 9)
            SomaDigitos = parseInt(SomaDigitos.toString().charAt(1));

        var DigitoV: any = 10 - SomaDigitos;
        if (DigitoV == 10) DigitoV = 0;

        DigitoV = DigitoV.toString();
        return DigitoV == dv ? true : false;
    }

    public validaDVContaBanrisul(conta, dv) {
        conta = this.pad(conta, 9);
        var SomaDigitos =
            conta.charAt(0) * 3 +
            conta.charAt(1) * 2 +
            conta.charAt(2) * 4 +
            conta.charAt(3) * 7 +
            conta.charAt(4) * 6 +
            conta.charAt(5) * 5 +
            conta.charAt(6) * 4 +
            conta.charAt(7) * 3 +
            conta.charAt(8) * 2;

        var Resto = SomaDigitos % 11;
        var DigitoV;
        if (Resto == 0) DigitoV = 0;
        else if (Resto == 1) DigitoV = 6;
        else DigitoV = 11 - Resto;

        DigitoV = DigitoV.toString();
        return DigitoV == dv ? true : false;
    }

    public validaDVContaCaixa(agencia, conta, dv) {
        if (dv.length != 1) return false;
        agencia = this.pad(agencia, 4);
        conta = this.pad(conta, 11);
        if (conta.charAt(2) == 0) {
            conta = conta.substr(0, 2) + "1" + conta.substr(3);
        }
        var SomaDigitos =
            agencia.charAt(0) * 8 +
            agencia.charAt(1) * 7 +
            agencia.charAt(2) * 6 +
            agencia.charAt(3) * 5 +
            conta.charAt(0) * 4 +
            conta.charAt(1) * 3 +
            conta.charAt(2) * 2 +
            conta.charAt(3) * 9 +
            conta.charAt(4) * 8 +
            conta.charAt(5) * 7 +
            conta.charAt(6) * 6 +
            conta.charAt(7) * 5 +
            conta.charAt(8) * 4 +
            conta.charAt(9) * 3 +
            conta.charAt(10) * 2;

        SomaDigitos = SomaDigitos * 10;
        var Resto = SomaDigitos % 11;
        var DigitoV: any = Resto == 10 ? 0 : Resto;

        DigitoV = DigitoV.toString();
        return DigitoV == dv ? true : false;
    }

    public validaDVContaBradesco(conta, dv) {
        if (dv.length != 1) return false;

        conta = this.pad(conta, 7);

        var SomaDigitos =
            conta.charAt(0) * 2 +
            conta.charAt(1) * 7 +
            conta.charAt(2) * 6 +
            conta.charAt(3) * 5 +
            conta.charAt(4) * 4 +
            conta.charAt(5) * 3 +
            conta.charAt(6) * 2;

        var Resto = SomaDigitos % 11;
        var DigitoV;
        if (Resto == 0) DigitoV = 0;
        else if (Resto == 1) DigitoV = "P";
        else DigitoV = 11 - Resto;

        DigitoV = DigitoV.toString();
        return DigitoV == dv ? true : false;
    }

    public validaDVContaItau(agencia, conta, dv) {
        var Digito1: any = (agencia.charAt(0) * 2).toString();
        if (Digito1 > 9)
            Digito1 = parseInt(Digito1.charAt(0)) + parseInt(Digito1.charAt(1));

        var Digito2: any = (agencia.charAt(1) * 1).toString();
        if (Digito2 > 9)
            Digito2 = parseInt(Digito2.charAt(0)) + parseInt(Digito2.charAt(1));

        var Digito3: any = (agencia.charAt(2) * 2).toString();
        if (Digito3 > 9)
            Digito3 = parseInt(Digito3.charAt(0)) + parseInt(Digito3.charAt(1));

        var Digito4: any = (agencia.charAt(3) * 1).toString();
        if (Digito4 > 9)
            Digito4 = parseInt(Digito4.charAt(0)) + parseInt(Digito4.charAt(1));

        var Digito5: any = (conta.charAt(0) * 2).toString();
        if (Digito5 > 9)
            Digito5 = parseInt(Digito5.charAt(0)) + parseInt(Digito5.charAt(1));

        var Digito6: any = (conta.charAt(1) * 1).toString();
        if (Digito6 > 9)
            Digito6 = parseInt(Digito6.charAt(0)) + parseInt(Digito6.charAt(1));

        var Digito7: any = (conta.charAt(2) * 2).toString();
        if (Digito7 > 9)
            Digito7 = parseInt(Digito7.charAt(0)) + parseInt(Digito7.charAt(1));

        var Digito8: any = (conta.charAt(3) * 1).toString();
        if (Digito8 > 9)
            Digito8 = parseInt(Digito8.charAt(0)) + parseInt(Digito8.charAt(1));

        var Digito9: any = (conta.charAt(4) * 2).toString();
        if (Digito9 > 9)
            Digito9 = parseInt(Digito9.charAt(0)) + parseInt(Digito9.charAt(1));

        var SomaDigitos =
            parseInt(Digito1) +
            parseInt(Digito2) +
            parseInt(Digito3) +
            parseInt(Digito4) +
            parseInt(Digito5) +
            parseInt(Digito6) +
            parseInt(Digito7) +
            parseInt(Digito8) +
            parseInt(Digito9);

        var Resto = SomaDigitos % 10;
        var DigitoV: any = Resto == 0 ? 0 : 10 - Resto;

        DigitoV = DigitoV.toString();
        return DigitoV == dv ? true : false;
    }

    public validaDVContaReal(agencia, conta, dv) {
        if (dv.length != 1) return false;

        var SomaDigitos =
            agencia.charAt(0) * 8 +
            agencia.charAt(1) * 1 +
            agencia.charAt(2) * 4 +
            agencia.charAt(3) * 7 +
            conta.charAt(0) * 2 +
            conta.charAt(1) * 2 +
            conta.charAt(2) * 5 +
            conta.charAt(3) * 9 +
            conta.charAt(4) * 3 +
            conta.charAt(5) * 9 +
            conta.charAt(6) * 5;

        var Resto = SomaDigitos % 11;
        var DigitoV;

        if (Resto == 0) DigitoV = 1;
        else if (Resto == 1) DigitoV = 0;
        else DigitoV = 11 - Resto;

        DigitoV = DigitoV.toString();
        return DigitoV == dv ? true : false;
    }

    public validaDVContaHSBC(agencia, conta, dv) {
        if (dv.length != 1) return false;

        var SomaDigitos =
            agencia.charAt(0) * 8 +
            agencia.charAt(1) * 9 +
            agencia.charAt(2) * 2 +
            agencia.charAt(3) * 3 +
            conta.charAt(0) * 4 +
            conta.charAt(1) * 5 +
            conta.charAt(2) * 6 +
            conta.charAt(3) * 7 +
            conta.charAt(4) * 8 +
            conta.charAt(5) * 9;

        var Resto = SomaDigitos % 11;
        var DigitoV: any = Resto == 0 || Resto == 10 ? 0 : Resto;

        DigitoV = DigitoV.toString();
        return DigitoV == dv ? true : false;
    }

    public validaDVContaCitibank(conta, dv) {
        if (dv.length != 1) return false;

        var SomaDigitos =
            conta.charAt(0) * 11 +
            conta.charAt(1) * 10 +
            conta.charAt(2) * 9 +
            conta.charAt(3) * 8 +
            conta.charAt(4) * 7 +
            conta.charAt(5) * 6 +
            conta.charAt(6) * 5 +
            conta.charAt(7) * 4 +
            conta.charAt(8) * 3 +
            conta.charAt(9) * 2;

        var Resto = SomaDigitos % 11;
        var DigitoV: any = Resto <= 1 ? 0 : 11 - Resto;

        DigitoV = DigitoV.toString();
        return DigitoV == dv ? true : false;
    }
}
