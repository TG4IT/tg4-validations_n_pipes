import { PipeTransform } from '@angular/core';
export declare class CnpjFormat implements PipeTransform {
    transform(cnpj: string): string;
}
