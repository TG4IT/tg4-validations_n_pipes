import { Pipe } from '@angular/core';
var CpfFormat = (function () {
    function CpfFormat() {
    }
    CpfFormat.prototype.transform = function (cpf) {
        if (!cpf || cpf.trim() === '') {
            return '';
        }
        var formatted;
        formatted = cpf.substring(0, 3) + '.';
        formatted = formatted + cpf.substring(3, 6) + '.';
        formatted = formatted + cpf.substring(6, 9) + '-';
        formatted = formatted + cpf.substring(9, 11);
        return formatted;
    };
    ;
    return CpfFormat;
}());
export { CpfFormat };
CpfFormat.decorators = [
    { type: Pipe, args: [{
                name: 'cpfFormat'
            },] },
];
/** @nocollapse */
CpfFormat.ctorParameters = function () { return []; };
//# sourceMappingURL=cpf.pipe.js.map