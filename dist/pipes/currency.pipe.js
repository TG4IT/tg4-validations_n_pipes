import { Pipe } from '@angular/core';
var CurrencyFormat = (function () {
    function CurrencyFormat() {
    }
    CurrencyFormat.prototype.transform = function (value, currencySign, chunkDelimiter, decimalDelimiter) {
        if (currencySign === void 0) { currencySign = 'R$ '; }
        if (chunkDelimiter === void 0) { chunkDelimiter = '.'; }
        if (decimalDelimiter === void 0) { decimalDelimiter = ','; }
        if (!value) {
            return 'R$ 0,00';
        }
        var changedValue = this.addCommas(value.toFixed(2));
        var formatted = changedValue.toString().replace(/[,.]/g, function (val) {
            return val === ',' ? chunkDelimiter : decimalDelimiter;
        });
        return currencySign + formatted;
    };
    ;
    CurrencyFormat.prototype.addCommas = function (nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    };
    ;
    return CurrencyFormat;
}());
export { CurrencyFormat };
CurrencyFormat.decorators = [
    { type: Pipe, args: [{
                name: 'currencyFormat'
            },] },
];
/** @nocollapse */
CurrencyFormat.ctorParameters = function () { return []; };
//# sourceMappingURL=currency.pipe.js.map