import { Pipe } from '@angular/core';
var CepFormat = (function () {
    function CepFormat() {
    }
    CepFormat.prototype.transform = function (cep) {
        if (!cep || cep.trim() === '') {
            return '';
        }
        var formatted;
        formatted = cep.substring(0, 5) + '-';
        formatted = formatted + cep.substring(5, 8);
        return formatted;
    };
    ;
    return CepFormat;
}());
export { CepFormat };
CepFormat.decorators = [
    { type: Pipe, args: [{
                name: 'cepFormat'
            },] },
];
/** @nocollapse */
CepFormat.ctorParameters = function () { return []; };
//# sourceMappingURL=cep.pipe.js.map