import { PipeTransform } from '@angular/core';
export declare class CpfFormat implements PipeTransform {
    transform(cpf: string): string;
}
