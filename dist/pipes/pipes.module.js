import { NgModule } from '@angular/core';
import { CurrencyFormat } from './currency.pipe';
import { CpfFormat } from './cpf.pipe';
import { CnpjFormat } from './cnpj.pipe';
import { CepFormat } from './cep.pipe';
var PipesModule = (function () {
    function PipesModule() {
    }
    return PipesModule;
}());
export { PipesModule };
PipesModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    CurrencyFormat,
                    CpfFormat,
                    CnpjFormat,
                    CepFormat
                ],
                exports: [
                    CurrencyFormat,
                    CpfFormat,
                    CnpjFormat,
                    CepFormat
                ]
            },] },
];
/** @nocollapse */
PipesModule.ctorParameters = function () { return []; };
//# sourceMappingURL=pipes.module.js.map