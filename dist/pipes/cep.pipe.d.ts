import { PipeTransform } from '@angular/core';
export declare class CepFormat implements PipeTransform {
    transform(cep: string): string;
}
