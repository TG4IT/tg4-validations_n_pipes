import { Pipe } from '@angular/core';
var CnpjFormat = (function () {
    function CnpjFormat() {
    }
    CnpjFormat.prototype.transform = function (cnpj) {
        if (!cnpj || cnpj.trim() === '') {
            return '';
        }
        var formatted;
        formatted = cnpj.substring(0, 2) + '.';
        formatted = formatted + cnpj.substring(2, 5) + '.';
        formatted = formatted + cnpj.substring(5, 8) + '/';
        formatted = formatted + cnpj.substring(8, 12) + '-';
        formatted = formatted + cnpj.substring(12, 14);
        return formatted;
    };
    ;
    return CnpjFormat;
}());
export { CnpjFormat };
CnpjFormat.decorators = [
    { type: Pipe, args: [{
                name: 'cnpjFormat'
            },] },
];
/** @nocollapse */
CnpjFormat.ctorParameters = function () { return []; };
//# sourceMappingURL=cnpj.pipe.js.map