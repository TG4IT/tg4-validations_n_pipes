import { PipeTransform } from '@angular/core';
export declare class CurrencyFormat implements PipeTransform {
    transform(value: number, currencySign?: string, chunkDelimiter?: string, decimalDelimiter?: string): string;
    private addCommas(nStr);
}
