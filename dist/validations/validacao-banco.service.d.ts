export declare class ValidacaoBancoService {
    private pad(str, max);
    private checkAllZeros(agencia_conta);
    validaDVAgenciaBancoDoBrasil(agencia: any, dv: any): boolean;
    validaDVAgenciaBanrisul(agencia: any, dv: any): boolean;
    calcula2ndDigitoBanrisul(agencia: any, DV1: any): any;
    validaDVAgenciaBradesco(agencia: any, dv: any): boolean;
    validaAgenciaBancoob(agencia: any): boolean;
    validaAgenciaBradesco(agencia: any): boolean;
    validaAgenciaItau(agencia: any): boolean;
    validaAgenciaBancodoBrasil(agencia: any): boolean;
    validaAgenciaSantander(agencia: any): boolean;
    validaAgenciaCaixa(agencia: any): boolean;
    validaContaBancoob(conta: any): boolean;
    validaContaBradesco(conta: any): boolean;
    validaContaItau(conta: any): boolean;
    validaContaBancodoBrasil(conta: any): boolean;
    validaContaSantander(conta: any): boolean;
    validaContaCaixa(conta: any): boolean;
    validaDVContaBancoDoBrasil(conta: any, dv: any): boolean;
    validaDVContaSantander(agencia: any, conta: any, dv: any): boolean;
    validaDVContaBanrisul(conta: any, dv: any): boolean;
    validaDVContaCaixa(agencia: any, conta: any, dv: any): boolean;
    validaDVContaBradesco(conta: any, dv: any): boolean;
    validaDVContaItau(agencia: any, conta: any, dv: any): boolean;
    validaDVContaReal(agencia: any, conta: any, dv: any): boolean;
    validaDVContaHSBC(agencia: any, conta: any, dv: any): boolean;
    validaDVContaCitibank(conta: any, dv: any): boolean;
}
