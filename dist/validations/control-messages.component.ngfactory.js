/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
/* tslint:disable */
import * as i0 from '@angular/core';
import * as i1 from '@angular/common';
import * as i2 from './control-messages.component';
var styles_ControlMessages = [];
export var RenderType_ControlMessages = i0.ɵcrt({ encapsulation: 2,
    styles: styles_ControlMessages, data: {} });
function View_ControlMessages_1(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, null, null, 1, 'p', [], null, null, null, null, null)), (_l()(),
            i0.ɵted(null, ['', '']))], null, function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = _co.errorMessage;
        _ck(_v, 1, 0, currVal_0);
    });
}
export function View_ControlMessages_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵand(16777216, null, null, 1, null, View_ControlMessages_1)), i0.ɵdid(16384, null, 0, i1.NgIf, [i0.ViewContainerRef,
            i0.TemplateRef], { ngIf: [0, 'ngIf'] }, null)], function (_ck, _v) {
        var _co = _v.component;
        var currVal_0 = (_co.errorMessage !== null);
        _ck(_v, 1, 0, currVal_0);
    }, null);
}
export function View_ControlMessages_Host_0(_l) {
    return i0.ɵvid(0, [(_l()(), i0.ɵeld(0, null, null, 1, 'control-messages', [], null, null, null, View_ControlMessages_0, RenderType_ControlMessages)), i0.ɵdid(49152, null, 0, i2.ControlMessages, [], null, null)], null, null);
}
export var ControlMessagesNgFactory = i0.ɵccf('control-messages', i2.ControlMessages, View_ControlMessages_Host_0, { control: 'control' }, {}, []);
//# sourceMappingURL=control-messages.component.ngfactory.js.map