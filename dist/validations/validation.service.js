import { Injectable } from '@angular/core';
import { validarCPF, validarCNPJ, validarPisPasep, validarDeclaracaoNascidoVivo, validarCartaoNacionalSaude } from '../functions/document/document.validation';
import { validarEmail, validarTelefoneCelular, validarTelefoneFixo } from '../functions/user-info/user-info.validation';
import { retirarNumeros, retirarNumerosMenosTraco, retirarCaracterEspecialMenosTraco, retirarCaracterEspecial, retirarLetras, transformarLetrasAcentuadas } from '../functions/cleaners/cleaners';
var ValidationService = (function () {
    function ValidationService() {
    }
    ValidationService.getValidatorErrorMessage = function (validatorName, validatorValue) {
        var config = {
            'required': 'Campo Obrigatório.',
            'invalidCpf': 'Digite um CPF válido.',
            'invalidPeriod': 'A data escolhida deve ser menor que a data de hoje.',
            'invalidEmail': 'E-mail inválido.',
            'invalidCep': 'Formato de CEP inválido.',
            'requiredCheckBox': 'Campo obrigatório',
            'minlength': "Tamanho m\u00EDnimo " + validatorValue.requiredLength + ".",
            'maxlength': "Tamanho m\u00E1ximo " + validatorValue.requiredLength + ".",
            'lengthNameError': 'Digite seu nome completo.',
            'invalidTel': 'Telefone inválido.',
            'invalidCnpj': 'O CNPJ informado é inválido. Por favor, verifique e atualize a informação.',
            'nameError': 'Digite o nome corretamente.',
            'invalidText': 'O texto não pode conter caracter especial.',
            'invalidPassword': 'A senha deve ter mais do que 6 caracteres e conter letra e número.',
            'textError': 'Somente letras são permitidas',
            'invalidDeclaracaoNascidoVivo': 'Digite um nº de Declaração Nascido Vivo válido.',
            'invalidCartaoNacionalSaude': 'Digite um nº de Cartão Nacional de Saúde válido.',
            'invalidPisPasep': 'Digite um nº de PIS/PASEP válido.'
        };
        return config[validatorName];
    };
    ValidationService.validarCNPJ = function (control) {
        if (verificarControlInvalido(control))
            return null;
        return validarCNPJ(control.value) ? null : { 'invalidCnpj': true };
    };
    ValidationService.validarCPF = function (control) {
        if (verificarControlInvalido(control))
            return null;
        return validarCPF(control.value) ? null : { 'invalidCpf': true };
    };
    ValidationService.validarPisPasep = function (control) {
        if (verificarControlInvalido(control))
            return null;
        return validarPisPasep(control.value) ? null : { 'invalidPisPasep': true };
    };
    ValidationService.validarDeclaracaoNascidoVivo = function (control) {
        if (verificarControlInvalido(control))
            return null;
        return validarDeclaracaoNascidoVivo(control.value) ? null : { 'invalidDeclaracaoNascidoVivo': true };
    };
    ValidationService.validarCartaoNacionalSaude = function (control) {
        if (verificarControlInvalido(control))
            return null;
        return validarCartaoNacionalSaude(control.value) ? null : { 'invalidCartaoNacionalSaude': true };
    };
    ValidationService.validarEmail = function (control) {
        if (verificarControlInvalido(control))
            return null;
        return validarEmail(control.value) ? null : { 'invalidEmail': true };
    };
    ValidationService.validarNome = function (control) {
        if (verificarControlInvalido(control))
            return null;
        var nome = control.value.replace(/[^a-zA-ZáéíóúàèìòùâêîôûãõçÁÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃÕÇ\s]/g, '');
        if (control.value !== nome)
            control.setValue(nome);
        return validarEmail(nome) ? null : { 'nameError': true };
    };
    ValidationService.validarCep = function (control) {
        if (verificarControlInvalido(control))
            return null;
        return validarEmail(control.value) ? null : { 'invalidCep': true };
    };
    ValidationService.retirarNumeros = function (control) {
        if (verificarControlInvalido(control))
            return null;
        var valor = retirarNumeros(control.value);
        atualizarControl(control, valor);
    };
    ValidationService.retirarNumerosMenosTraco = function (control) {
        if (verificarControlInvalido(control))
            return null;
        var valor = retirarNumerosMenosTraco(control.value);
        atualizarControl(control, valor);
    };
    ValidationService.retirarCaracterEspecialMenosTraco = function (control) {
        if (verificarControlInvalido(control))
            return null;
        var valor = retirarCaracterEspecialMenosTraco(control.value);
        atualizarControl(control, valor);
    };
    ValidationService.retirarCaracterEspecial = function (control) {
        if (verificarControlInvalido(control))
            return null;
        var nome = retirarCaracterEspecial(control.value);
        if (nome && (control.value !== nome))
            control.patchValue(nome, { emitEvent: false });
    };
    ValidationService.retirarLetras = function (control) {
        if (verificarControlInvalido(control))
            return null;
        var valor = retirarLetras(control.value);
        atualizarControl(control, valor);
    };
    ValidationService.transformarLetrasAcentuadas = function (control) {
        if (verificarControlInvalido(control))
            return null;
        return transformarLetrasAcentuadas(control.value);
    };
    ValidationService.validarTelefoneCelular = function (control) {
        if (verificarControlInvalido(control))
            return null;
        return validarTelefoneCelular(control.value) ? null : { 'invalidTel': true };
    };
    ValidationService.validarTelefoneFixo = function (control) {
        if (verificarControlInvalido(control))
            return null;
        return validarTelefoneFixo(control.value) ? null : { 'invalidTel': true };
    };
    return ValidationService;
}());
export { ValidationService };
ValidationService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ValidationService.ctorParameters = function () { return []; };
function verificarControlInvalido(control) {
    return !control || !control.value;
}
function atualizarControl(control, valor) {
    if (control.value !== valor)
        control.setValue(valor);
}
//# sourceMappingURL=validation.service.js.map