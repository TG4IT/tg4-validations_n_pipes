import { Component, Input } from '@angular/core';
import { ValidationService } from './validation.service';
var ControlMessages = (function () {
    function ControlMessages() {
    }
    Object.defineProperty(ControlMessages.prototype, "errorMessage", {
        get: function () {
            for (var propertyName in this.control.errors) {
                if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched)
                    return ValidationService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    return ControlMessages;
}());
export { ControlMessages };
ControlMessages.decorators = [
    { type: Component, args: [{
                selector: 'control-messages',
                template: "<p *ngIf=\"errorMessage !== null\" >{{errorMessage}}</p>"
            },] },
];
/** @nocollapse */
ControlMessages.ctorParameters = function () { return []; };
ControlMessages.propDecorators = {
    'control': [{ type: Input },],
};
//# sourceMappingURL=control-messages.component.js.map