import { FormControl } from '@angular/forms';
export declare class ControlMessages {
    control: FormControl;
    constructor();
    readonly errorMessage: string;
}
