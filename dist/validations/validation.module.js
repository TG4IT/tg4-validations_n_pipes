import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ControlMessages } from './control-messages.component';
import { ValidationService } from './validation.service';
var ValidationModule = (function () {
    function ValidationModule() {
    }
    return ValidationModule;
}());
export { ValidationModule };
ValidationModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ControlMessages],
                imports: [BrowserModule],
                exports: [ControlMessages],
                providers: [ValidationService]
            },] },
];
/** @nocollapse */
ValidationModule.ctorParameters = function () { return []; };
//# sourceMappingURL=validation.module.js.map