export declare class ValidationService {
    constructor();
    static getValidatorErrorMessage(validatorName: string, validatorValue?: any): string;
    static validarCNPJ(control: any): {
        'invalidCnpj': boolean;
    };
    static validarCPF(control: any): {
        'invalidCpf': boolean;
    };
    static validarPisPasep(control: any): {
        'invalidPisPasep': boolean;
    };
    static validarDeclaracaoNascidoVivo(control: any): {
        'invalidDeclaracaoNascidoVivo': boolean;
    };
    static validarCartaoNacionalSaude(control: any): {
        'invalidCartaoNacionalSaude': boolean;
    };
    static validarEmail(control: any): {
        'invalidEmail': boolean;
    };
    static validarNome(control: any): {
        'nameError': boolean;
    };
    static validarCep(control: any): {
        'invalidCep': boolean;
    };
    static retirarNumeros(control: any): any;
    static retirarNumerosMenosTraco(control: any): any;
    static retirarCaracterEspecialMenosTraco(control: any): any;
    static retirarCaracterEspecial(control: any): any;
    static retirarLetras(control: any): any;
    static transformarLetrasAcentuadas(control: any): any;
    static validarTelefoneCelular(control: any): {
        'invalidTel': boolean;
    };
    static validarTelefoneFixo(control: any): {
        'invalidTel': boolean;
    };
}
